<?php
/**
 * LICENSE
 *
 * This file is part of CFPropertyList.
 *
 * Copyright (c) 2018 Teclib'
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * ------------------------------------------------------------------------------
 * @author    Rodney Rehm <rodney.rehm@medialize.de>
 * @author    Christian Kruse <cjk@wwwtech.de>
 * @copyright Copyright © 2018 Teclib
 * @package   CFPropertyList
 * @license   MIT
 * @link      https://github.com/TECLIB/CFPropertyList/
 * @link      http://developer.apple.com/documentation/Darwin/Reference/ManPages/man5/plist.5.html Property Lists
 */

namespace CFPropertyList;

use Iterator;
use DOMDocument;
use DOMException;
use DOMImplementation;
use DOMNode;

/**
 * Property List
 * Interface for handling reading, editing and saving Property Lists as defined by Apple.
 *
 * @example   example-read-01.php Read an XML PropertyList
 * @example   example-read-03.php Read a PropertyList without knowing the type
 * @example   example-create-01.php Using the CFPropertyList API
 * @example   example-create-02.php Using CFTypeDetector
 * @example   example-create-03.php Using CFTypeDetector with CFDate and CFData
 * @example   example-modify-01.php Read, modify and save a PropertyList
 * ------------------------------------------------------------------------------
 */
class CFPropertyList implements Iterator {
  /**
   * Path of PropertyList
   * @var string
   */
    protected $file = null;

  /**
   * CFType nodes
   * @var array
   */
    protected $value = array();

  /**
   * Position of iterator {@link http://php.net/manual/en/class.iterator.php}
   * @var integer
   */
    protected $iteratorPosition = 0;

  /**
   * List of Keys for numerical iterator access {@link http://php.net/manual/en/class.iterator.php}
   * @var array
   */
    protected $iteratorKeys = null;

  /**
   * List of NodeNames to ClassNames for resolving plist-files
   * @var array
   */
    protected static $types = array(
    'string'  => 'CFString',
    'real'    => 'CFNumber',
    'integer' => 'CFNumber',
    'date'    => 'CFDate',
    'true'    => 'CFBoolean',
    'false'   => 'CFBoolean',
    'data'    => 'CFData',
    'array'   => 'CFArray',
    'dict'    => 'CFDictionary'
    );

    /**
     * Create new CFPropertyList.
     * If a path to a PropertyList is specified, it is loaded automatically.
     *
     * @param string $file Path of PropertyList
     *
     * @throws IOException if file could not be read by {@link load()}
     * @throws PListException
     * @throws DOMException
     *
     * @uses $file for storing the current file, if specified
     * @uses load() for loading the plist-file
     */
    public function __construct($file = null) {
        $this->file = $file;

        if ($this->file)
            $this->load();
    }

    /**
     * Load an XML PropertyList.
     *
     * @param string $file Path of PropertyList, defaults to {@link $file}
     *
     * @throws DOMException
     * @throws IOException
     * @throws PListException
     *
     * @uses load() to actually load the file
     */
    public function loadXML($file = null) {
        $this->load($file);
    }

    /**
     * Load an XML PropertyList.
     *
     * @param resource $stream A stream containing the xml document.
     *
     * @return void
     *
     * @throws DOMException if XML-stream could not be read properly
     * @throws IOException if stream could not be read
     * @throws PListException
     */
    public function loadXMLStream($stream) {
        $contents = stream_get_contents($stream);

        if ($contents === false)
            throw IOException::notReadable('<stream>');

        $this->parse($contents);
    }

    /**
     * Load a plist file.
     * Load and import a plist file.
     *
     * @param string $file Path of PropertyList, defaults to {@link $file}
     *
     * @return void
     *
     * @throws DOMException if plist file could not be parsed properly
     * @throws IOException if file could not be read
     * @throws PListException
     *
     * @uses $file if argument $file was not specified
     * @uses $value reset to empty array
     * @uses import() for importing the values
     */
    public function load($file = null) {
        $file = $file ? $file : $this->file;
        $this->value = array();

        if (!is_readable($file))
            throw IOException::notReadable($file);

        $doc = new DOMDocument();
        $prevXmlErrors = libxml_use_internal_errors(true);

        libxml_clear_errors();

        if (!$doc->load($file)) {
            $message = $this->getLibxmlErrors();

            libxml_clear_errors();
            libxml_use_internal_errors($prevXmlErrors);

            throw new DOMException($message);
        }

        libxml_use_internal_errors($prevXmlErrors);

        $this->import($doc->documentElement, $this);
    }

    /**
     * Parse a plist string.
     * Parse and import a plist string.
     *
     * @param string $str String containing the PropertyList, defaults to {@link $content}
     *
     * @return void
     *
     * @throws DOMException if plist file could not be parsed properly
     * @throws IOException if file could not be read
     * @throws PListException
     *
     * @uses $content if argument $str was not specified
     * @uses $value reset to empty array
     * @uses import() for importing the values
     */
    public function parse($str = null) {
        $str = $str !== null ? $str : $this->content;

        if ($str === null || strlen($str) === 0)
            throw IOException::readError('');

        $this->value = array();

        $doc = new DOMDocument();
        $prevXmlErrors = libxml_use_internal_errors(true);

        libxml_clear_errors();

        if (!$doc->loadXML($str)) {
            $message = $this->getLibxmlErrors();

            libxml_clear_errors();
            libxml_use_internal_errors($prevXmlErrors);

            throw new DOMException($message);
        }

        libxml_use_internal_errors($prevXmlErrors);

        $this->import($doc->documentElement, $this);
    }

    protected function getLibxmlErrors() {
        return implode(', ', array_map(function (\LibXMLError $error) {
            return trim("{$error->line}:{$error->column} [$error->code] $error->message");
        }, libxml_get_errors()));
    }

    /**
     * Convert a DOMNode into a CFType.
     *
     * @param DOMNode $node Node to import children of
     * @param CFDictionary|CFArray|CFPropertyList $parent
     *
     * @return void
     *
     * @throws PListException
     */
    protected function import(DOMNode $node, $parent) {
        // abort if there are no children
        if (!$node->childNodes->length)
            return;

        foreach ($node->childNodes as $n) {
            // skip if we can't handle the element
            if (!isset(self::$types[$n->nodeName]))
                continue;

            $class = __NAMESPACE__ . '\\'.self::$types[$n->nodeName];
            $key = null;

            // find previous <key> if possible
            $ps = $n->previousSibling;
            while ($ps && $ps->nodeName == '#text' && $ps->previousSibling)
                $ps = $ps->previousSibling;

            // read <key> if possible
            if ($ps && $ps->nodeName == 'key')
                $key = $ps->firstChild->nodeValue;

            switch ($n->nodeName) {
                case 'date':
                    $value = new $class(CFDate::dateValue($n->nodeValue));
                    break;
                case 'data':
                    $value = new $class($n->nodeValue, true);
                    break;
                case 'string':
                    $value = new $class($n->nodeValue);
                    break;
                case 'real':
                case 'integer':
                    if (substr($n->nodeValue, 0, 2) === '0x')
                        $value = new $class($n->nodeValue);
                    else
                        $value = new $class($n->nodeName == 'real' ? floatval($n->nodeValue) : intval($n->nodeValue));
                    break;
                case 'true':
                case 'false':
                    $value = new $class($n->nodeName == 'true');
                    break;
                case 'array':
                case 'dict':
                    $value = new $class();
                    $this->import($n, $value);

                    if ($value instanceof CFDictionary) {
                        $hsh = $value->getValue();

                        if (isset($hsh['CF$UID']) && count($hsh) == 1)
                            $value = new CFUid($hsh['CF$UID']->getValue());
                    }
                    break;
            }

            if ($parent instanceof CFDictionary) // Dictionaries need a key
                $parent->add($key, $value);
            else // others don't
                $parent->add($value);
        }
    }

  /**
   * Convert CFPropertyList to XML
   *
   * @param bool $formatted Print plist formatted (i.e. with newlines and whitespace indention) if true; defaults to false
   *
   * @return string The XML content
   */
    public function toXML($formatted = true) {
        $domimpl = new DOMImplementation();
        $dtd = $domimpl->createDocumentType('plist', '-//Apple//DTD PLIST 1.0//EN', 'http://www.apple.com/DTDs/PropertyList-1.0.dtd');
        $doc = $domimpl->createDocument(null, "plist", $dtd);

        $doc->encoding = "UTF-8";

        if ($formatted) {
            $doc->formatOutput = true;
            $doc->preserveWhiteSpace = true;
        }

        $plist = $doc->documentElement;

        $plist->setAttribute('version', '1.0');
        $plist->appendChild($this->getValue(true)->toXML($doc));

        $plistCont = $doc->saveXML();

        if ($formatted) { // indentation
            $plistCont = preg_replace_callback('/^\s+</m', function ($matches) {
                $len = strlen($matches[0]) - 1;

                if ($len === 2) { // first or last tag
                    return str_replace("  ", '', $matches[0]);

                } else {
                    $tabsC = ($len * 2) - 4;
                    $nlen = abs($len - $tabsC);

                    while ($nlen--)
                        $matches[0] = ' '.$matches[0];

                    return str_replace("    ", "\t", $matches[0]);
                }
            }, $plistCont);
        }

        return $plistCont;
    }

  /************************************************************************************************
   *    M A N I P U L A T I O N
   ************************************************************************************************/

  /**
   * Add CFType to collection.
   *
   * @param CFType $value CFType to add to collection
   *
   * @return void
   *
   * @uses $value for adding $value
   */
    public function add(CFType $value = null) {
        // anything but CFType is null, null is an empty string - sad but true
        if (!$value)
            $value = new CFString();

        $this->value[] = $value;
    }

  /**
   * Get CFType from collection.
   *
   * @param integer $key Key of CFType to retrieve from collection
   *
   * @return CFType CFType found at $key, null else
   *
   * @uses $value for retrieving CFType of $key
   */
    public function get($key) {
        if (isset($this->value[$key]))
            return $this->value[$key];

        return null;
    }

  /**
   * Generic getter (magic)
   *
   * @param integer $key Key of CFType to retrieve from collection
   *
   * @return CFType CFType found at $key, null else
   *
   * @author Sean Coates <sean@php.net>
   *
   * @link http://php.net/oop5.overloading
   */
    public function __get($key) {
        return $this->get($key);
    }

  /**
   * Remove CFType from collection.
   *
   * @param integer $key Key of CFType to removes from collection
   *
   * @return CFType removed CFType, null else
   *
   * @uses $value for removing CFType of $key
   */
    public function del($key) {
        if (!isset($this->value[$key]))
            return null;

        $t = $this->value[$key];

        unset($this->value[$key]);

        return $t;
    }

  /**
   * Empty the collection
   *
   * @return array the removed CFTypes
   *
   * @uses $value for removing CFType of $key
   */
    public function purge() {
        $t = $this->value;
        $this->value = array();

        return $t;
    }

  /**
   * Get first (and only) child, or complete collection.
   *
   * @param boolean $cftype if set to true returned value will be CFArray instead of an array in case of a collection
   *
   * @return CFType|array CFType or list of CFTypes known to the PropertyList
   *
   * @uses $value for retrieving CFTypes
   */
    public function getValue($cftype = false) {
        if (count($this->value) === 1) {
            $t = array_values($this->value);

            return $t[0];

        } else if ($cftype) {
            $t = new CFArray();

            foreach ($this->value as $value) {
                if ($value instanceof CFType)
                    $t->add($value);
            }

            return $t;
        }

        return $this->value;
    }

    /**
     * Create CFType-structure from guessing the data-types.
     * The functionality has been moved to the more flexible {@link CFTypeDetector} facility.
     *
     * @param mixed $value Value to convert to CFType
     * @param array $options Configuration for casting values [autoDictionary, suppressExceptions, objectToArrayMethod, castNumericStrings]
     *
     * @return CFType CFType based on guessed type
     *
     * @throws PListException
     *
     * @uses CFTypeDetector for actual type detection
     * @deprecated
     */
    public static function guess($value, $options = array())
    {
        static $t = null;

        if ($t === null)
            $t = new CFTypeDetector($options);

        return $t->toCFType($value);
    }


  /************************************************************************************************
   *    S E R I A L I Z I N G
   ************************************************************************************************/

  /**
   * Get PropertyList as array.
   *
   * @return mixed primitive value of first (and only) CFType, or array of primitive values of collection
   *
   * @uses $value for retrieving CFTypes
   */
    public function toArray() {
        $a = array();

        foreach ($this->value as $value)
            $a[] = $value->toArray();

        if (count($a) === 1)
            return $a[0];

        return $a;
    }


  /************************************************************************************************
   *    I T E R A T O R   I N T E R F A C E
   ************************************************************************************************/

  /**
   * Rewind {@link $iteratorPosition} to first position (being 0)
   *
   * @return void
   *
   * @link http://php.net/manual/en/iterator.rewind.php
   * @uses $iteratorPosition set to 0
   * @uses $iteratorKeys store keys of {@link $value}
   */
    public function rewind() {
        $this->iteratorPosition = 0;
        $this->iteratorKeys = array_keys($this->value);
    }

  /**
   * Get Iterator's current {@link CFType} identified by {@link $iteratorPosition}
   *
   * @return CFType current Item
   *
   * @link http://php.net/manual/en/iterator.current.php
   * @uses $iteratorPosition identify current key
   * @uses $iteratorKeys identify current value
   */
    public function current() {
        return $this->value[$this->iteratorKeys[$this->iteratorPosition]];
    }

  /**
   * Get Iterator's current key identified by {@link $iteratorPosition}
   *
   * @return string key of the current Item
   *
   * @link http://php.net/manual/en/iterator.key.php
   * @uses $iteratorPosition identify current key
   * @uses $iteratorKeys identify current value
   */
    public function key() {
        return $this->iteratorKeys[$this->iteratorPosition];
    }

  /**
   * Increment {@link $iteratorPosition} to address next {@see CFType}
   *
   * @return void
   *
   * @link http://php.net/manual/en/iterator.next.php
   * @uses $iteratorPosition increment by 1
   */
    public function next() {
        $this->iteratorPosition++;
    }

  /**
   * Test if {@link $iteratorPosition} addresses a valid element of {@link $value}
   *
   * @return boolean true if current position is valid, false else
   *
   * @link http://php.net/manual/en/iterator.valid.php
   * @uses $iteratorPosition test if within {@link $iteratorKeys}
   * @uses $iteratorPosition test if within {@link $value}
   */
    public function valid() {
        return isset($this->iteratorKeys[$this->iteratorPosition]) && isset($this->value[$this->iteratorKeys[$this->iteratorPosition]]);
    }
}
